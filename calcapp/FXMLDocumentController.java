package calcapp;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class FXMLDocumentController implements Initializable {
       
    Calculator calc;
    
    @FXML
    private TextField result;
    
    @FXML
    private TextField actual;
       
    @FXML
    private void buttonClick(javafx.event.ActionEvent event) {
        Button button = (Button)event.getSource();
        String buttonText = button.getText();
        calc.input(buttonText);
        Update();
    }

    @FXML
    void buttonOperatorClick(javafx.event.ActionEvent event) {
        Button operatorButton = (Button)event.getSource();
        calc.setOperation(operatorButton.getText());
        calc.operator();
        Update();
    }
    
    @FXML
    void buttonEqualsClick(javafx.event.ActionEvent event) {
        calc.equals();
        Update();
    }
    
    @FXML
    void buttonClearClick(javafx.event.ActionEvent event) {
        calc.clear();
        Update();
    }
    
    @FXML
    void buttonSignClick(javafx.event.ActionEvent event) {
        calc.signPressed();
        Update();
    }
    
    @FXML
    void buttonEraseClick(javafx.event.ActionEvent event) {
        calc.erase();
        Update();
    }

    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        calc = new Calculator();
    }    
    
    
    private void Update()
    {
        result.setText(calc.getOutputResult());
        actual.setText(calc.getCurrentState());
    }
}
