package calcapp;
public class Calculator {

    private String nextOperation="";
    private String currentOperation="";
    private double firstNumSave=0.0;
    private double secondNumSave=0.0;
    private boolean existSecondNum=false;
    private boolean isOpButtonPressed=false;
    private String outputResult = "0";
    private String currentState="";
    private String operation="";
    
    public Calculator() {
    }
    
    public String getOutputResult(){
        return outputResult; 
    }
       
    public void input(String numberInTag){
        if (checktOverFlow() == false)
            return;
        if (existSecondNum)
            clear(); 
        if ("0".equals(outputResult) && !",".equals(numberInTag))
            outputResult = numberInTag;
        else{
            if (outputResult.contains(",") && ",".equals(numberInTag))
               outputResult = outputResult + "";
            else
               outputResult += numberInTag;
        }
    }
    
    
    public void equals(){
        nextOperation = operation;
        operation = currentOperation;

        switch (operation){
            case "+":
                secondNumEnter();
                outputResult = Double.toString(firstNumSave + secondNumSave);
                break;
            case "-":
                secondNumEnter();
                outputResult = Double.toString(firstNumSave - secondNumSave);
                break;
            case "x":
                secondNumEnter();
               outputResult = Double.toString(firstNumSave * secondNumSave);
                break;
            case "÷":
                secondNumEnter();
                if (secondNumSave == 0)
                 outputResult = "Divide by Zero!";
                else
                 outputResult = Double.toString(firstNumSave / secondNumSave);
                break;
            case "%":
                secondNumEnter();
                outputResult = Double.toString(firstNumSave / 100 * secondNumSave);
                break;
            case "^2":
                outputResult = Double.toString(Math.pow(firstNumSave, 2));
                break;
            case "^":
                secondNumEnter();
                outputResult = Double.toString(Math.pow(firstNumSave, (double)secondNumSave));
                break;
            case "√":
                if (firstNumSave < 0)
                    outputResult = "Not a number!";
                else
                    outputResult = Double.toString(Math.sqrt(firstNumSave));
                break;
            case "1/x":
                outputResult = Double.toString((1 / firstNumSave));
                break;
            default:
                break;
        }
        existSecondNum = true;
        isOpButtonPressed = false;
        operation = nextOperation;
    }
      
    
    void firstNumEnter(){
        firstNumSave = Double.parseDouble(outputResult);
 
        if ("√".equals(operation))
            currentState = operation + "(" + outputResult + ")";
        else if ("1/x".equals(operation))
            currentState = "1÷" + "(" + outputResult + ")";
        else if ("x²".equals(operation)){
            operation = "^2";
            currentState = "(" + outputResult + ") " + operation + " ";
        }
        else if ("x^y".equals(operation)){
            operation = "^";
            currentState = "(" + outputResult + ") " + operation + " ";
        }
        else
            currentState = outputResult + " " + operation + " ";
    }
    
    void secondNumEnter()
    {
        secondNumSave = Double.parseDouble(outputResult);
        currentState +=  Double.toString(secondNumSave);
        existSecondNum = true;
    }
          
    public void operator(){
        if (!"".equals(outputResult)){
            if ("Divide by Zero!".equals(outputResult) || "Not a number!".equals(outputResult)){
                outputResult = "0";
            }
            if (firstNumSave != 0 && !"".equals(outputResult) && existSecondNum == false 
                && isOpButtonPressed)
            {
                equals();
            }
            firstNumEnter();
            existSecondNum = false;
            if("1/x".equals(operation) || "^2".equals(operation) || "√".equals(operation))
            {  
                outputResult = outputResult + "";
                existSecondNum = true;
            }
            else{
                outputResult = "";
            }
           isOpButtonPressed = true;
           currentOperation = operation;
        }
    }
    
    public void clear(){
        firstNumSave = 0;
        secondNumSave = 0;
        existSecondNum = false;
        isOpButtonPressed = false;
        outputResult = "0";
        currentState = "";
    }
    
    public void erase(){
        if (!"0".equals(outputResult)){
            if (outputResult.length() == 1){
                outputResult = "0";
            }
            else if (outputResult.length() > 0){
                outputResult = outputResult.substring(0, outputResult.length() - 1);
            }
        }
    }
    public void signPressed(){
        double number = Double.parseDouble(outputResult);
        if (number != 0){
            number = number * (-1);
            outputResult = Double.toString(number);
        }
    }
   
    private boolean checktOverFlow(){
        return outputResult.length() <= 18;
    }
    
    public String getOperation(){
        return operation; 
    }
        
    public void setOperation(String value){
        operation = value; 
    }
    
    public String getCurrentState(){
        return currentState; 
    }
        
    public void setCurrentState(String value){
        currentState = value; 
    }
}